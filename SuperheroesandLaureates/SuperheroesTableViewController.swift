//
//  SuperheroesTableViewController.swift
//  SuperheroesandLaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class SuperheroesTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Superheroes.shared.fetchSuperheroes()
        NotificationCenter.default.addObserver(self, selector:#selector(superHeroesDelivered(notification:)),name: NSNotification.Name("Superheroes - delivered"), object:nil)
    }
    
    @objc func superHeroesDelivered(notification: Notification){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Superheroes.shared.superheroes.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "SUPERHEROES"
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseId", for: indexPath)

         //Configure the cell...
        let myHero = Superheroes.shared.superheroes[indexPath.row]
        cell.textLabel?.text = "\(myHero.name) (aka: \(myHero.secretIdentity))"
        var powersText = ""
        for i in 0..<myHero.powers.count{
            if i == myHero.powers.count - 1  {
                powersText += myHero.powers[i]
            }else{
                powersText += "\(myHero.powers[i]), "
            }
        }
        cell.detailTextLabel?.text = powersText

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }


    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
