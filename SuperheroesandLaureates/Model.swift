//
//  Model.swift
//  SuperheroesandLaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation
struct Superhero: Codable{
    var squadName:String
    var homeTown:String
    var formed:Int
    var secretBase:String
    var active:Bool
    var members:[Members]
}

struct Members: Codable{
    var name: String
    var age: Int
    var secretIdentity: String
    var powers:[String]
}

class Superheroes {
    
    let superheroInfo = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"
    
    var superheroes : [Members] = []
    
    init(){
        superheroes = []
    }
    
    static var shared = Superheroes()
    func fetchSuperheroes() {
        let urlSession = URLSession.shared
        let url = URL(string: superheroInfo)
        urlSession.dataTask(with: url!, completionHandler: displaySuperheroesInTableView).resume()
    }
    
    func displaySuperheroesInTableView(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        
        do {
            let decoder:JSONDecoder = JSONDecoder()
            let hero = try decoder.decode(Superhero.self, from: data!)
            self.superheroes = hero.members
            NotificationCenter.default.post(name: NSNotification.Name("Superheroes - delivered"), object: nil)
            } catch {
            print(error)
        }
    }
    


}
